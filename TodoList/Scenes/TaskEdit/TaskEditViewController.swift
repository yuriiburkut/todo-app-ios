//
//  TaskEditViewController.swift
//  TodoList
//
//  Created by Yuriy Burkut on 21.01.2021.
//

import UIKit
import Validator
import CoreData

class TaskEditViewController : UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    //MARK: Properties
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var saveTaskButton: UIBarButtonItem!
    
    weak var tasksListDelegate: TasksListDelegate?
    let dismissKeyboardButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(hideKeyboard))
    var toolbarButtonItems = [UIBarButtonItem]()
    
    let titleValidationRule = ValidationRulePattern(pattern: "[a-zA-z0-9^ .]{1,50}$", error: TitleValidationError("Title must contain only letters and numbers"))
    
    var task: Task!
    var isTaskEditing: Bool = false
    var isTaskInputError: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleTextField.delegate = self
        descriptionTextView.delegate = self
        
        initToobarButtons()
        setExistedTask()
        updateSaveButtonState()
        setKeyboardObserver()
    }
    
    @IBAction func onTitleChanged(_ sender: UITextField) {
        if let text = sender.text {
            if titleValidationRule.validate(input: text) {
                dissmissTitleInputError()
                task.taskTitle = text
            } else {
                showTitleInputError()
            }
        }
        updateSaveButtonState()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    @IBAction func onSaveButtonClicked(_ sender: UIBarButtonItem) {
        if isTaskEditing {
            tasksListDelegate?.updateTask(task: task)
        } else {
            tasksListDelegate?.saveTask(task: task)
        }
        navigationController?.popViewController(animated: true)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView === descriptionTextView {
            if let text = textView.text {
                task.taskDescription = text
            }
            updateSaveButtonState()
        }
    }
    
    func setExistedTask() {
        if let receivedTask: Task = task {
            titleTextField.text = receivedTask.taskTitle
            descriptionTextView.text = receivedTask.taskDescription
        } else {
            task = Task(
                taskId: UUID.init().uuidString,
                taskDescription: "",
                taskTitle: "",
                isCompleted: false
            )
        }
    }

    func updateSaveButtonState() {
        saveTaskButton.isEnabled = !task.taskTitle.isEmpty && !task.taskDescription.isEmpty && !isTaskInputError
    }
    
    func setKeyboardObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDissapear), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func initToobarButtons() {
        if let items: [UIBarButtonItem] = navigationItem.rightBarButtonItems {
            toolbarButtonItems = items
            
            if isTaskEditing {
                toolbarButtonItems.append(UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(removeTask)))
                navigationItem.setRightBarButtonItems(toolbarButtonItems, animated: false)
            }
        }
    }
    
    @objc func keyboardWillAppear() {
        if toolbarButtonItems.firstIndex(of: dismissKeyboardButton) == nil {
            toolbarButtonItems.append(dismissKeyboardButton)
            navigationItem.setRightBarButtonItems(toolbarButtonItems, animated: false)
        }
    }
    
    @objc func keyboardWillDissapear() {
        if let index: Int = toolbarButtonItems.firstIndex(of: dismissKeyboardButton) {
            toolbarButtonItems.remove(at: index)
            navigationItem.setRightBarButtonItems(toolbarButtonItems, animated: false)
        }
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    @objc func removeTask() {
        tasksListDelegate?.removeTask(task: task)
        navigationController?.popViewController(animated: true)
    }
    
    func showTitleInputError() {
        titleTextField.layer.borderColor = UIColor.red.cgColor
        titleTextField.layer.borderWidth = 1.0
        isTaskInputError = true
    }
    
    func dissmissTitleInputError() {
        titleTextField.layer.borderColor = UIColor.black.cgColor
        titleTextField.layer.borderWidth = 0.0
        isTaskInputError = false
    }
}
