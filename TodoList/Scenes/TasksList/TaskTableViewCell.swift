//
//  TaskTableViewCell.swift
//  TodoList
//
//  Created by Yuriy Burkut on 22.01.2021.
//

import UIKit
import BEMCheckBox

class TaskTableViewCell: UITableViewCell, BEMCheckBoxDelegate {
    
    //MARK: Properties
    @IBOutlet weak var taskTitleLabel: UILabel!
    @IBOutlet weak var taskDescriptionLabel: UILabel!
    @IBOutlet weak var completionCheckbox: BEMCheckBox!
    
    var onCheckboxClicked: ((Bool) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        completionCheckbox.delegate = self
    }
    
    func bind(task: Task, onSelectedClicked: @escaping (Bool) -> Void) {
        taskTitleLabel.text = task.taskTitle
        taskDescriptionLabel.text = task.taskDescription
        completionCheckbox.on = task.isCompleted
        self.onCheckboxClicked = onSelectedClicked
    }
    
    func didTap(_ checkBox: BEMCheckBox) {
        if let listener = onCheckboxClicked {
            listener(checkBox.on)
        }
    }
}
