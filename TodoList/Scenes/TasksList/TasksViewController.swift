//
//  TasksTableViewController.swift
//  TodoList
//
//  Created by Yuriy Burkut on 22.01.2021.
//

import UIKit
import CoreData

class TasksViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, TasksListDelegate {
    
    //MARK: Properties
    @IBOutlet weak var tasksTableView: UITableView!
    
    var tasks: [Task] = []
    let cellIdentifier = "TaskTableViewCell"
    var repository: TasksRepository!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let application = UIApplication.shared.delegate as? AppDelegate {
            repository = TasksRepository(container: application.persistentContainer)
        }
        
        fetchTasks()
        self.tasksTableView.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TaskTableViewCell
        else {
            fatalError("The dequeued cell is not an instance of TaskTableViewCell")
        }
        cell.bind(task: tasks[indexPath.row]) { isSelected in
            self.tasks[indexPath.row].isCompleted = isSelected
            self.repository.updateTask(task: self.tasks[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let destinationVC = storyboard?.instantiateViewController(withIdentifier: "TaskEditViewController") as! TaskEditViewController
        destinationVC.tasksListDelegate = self
        destinationVC.task = tasks[indexPath.row]
        destinationVC.isTaskEditing = true
        
        navigationController?.pushViewController(destinationVC, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? TaskEditViewController {
            destinationVC.tasksListDelegate = self
        }
    }
    
    func fetchTasks() {
        repository.fetchTasks() { results in
            self.tasks = results
            self.tasksTableView.reloadData()
        }
    }
    
    func saveTask(task: Task) {
        repository.saveTask(task: task)
        tasks.append(task)
        let newIndexPath = IndexPath(row: tasks.count - 1, section: 0)
        tasksTableView.insertRows(at: [newIndexPath], with: .automatic)
    }
    
    func updateTask(task: Task) {
        if let index: Int = tasks.firstIndex(where: {$0.taskId == task.taskId}) {
            repository.updateTask(task: task)
            tasks[index] = task
            let indexPath = IndexPath(row: index, section: 0)
            tasksTableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }

    func removeTask(task: Task) {
        if let index: Int = tasks.firstIndex(where: {$0.taskId == task.taskId}) {
            repository.removeTask(task: task)
            tasks.remove(at: index)
            let indexPath = IndexPath(row: index, section: 0)
            tasksTableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
}
