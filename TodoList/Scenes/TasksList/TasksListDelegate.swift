//
//  TasksListDelegate.swift
//  TodoList
//
//  Created by Yuriy Burkut on 25.01.2021.
//

import Foundation
import CoreData

protocol TasksListDelegate: AnyObject {
    func saveTask(task: Task)
    func updateTask(task: Task)
    func removeTask(task: Task)
}
