//
//  Task.swift
//  TodoList
//
//  Created by Yuriy Burkut on 28.01.2021.
//

import Foundation

struct Task: Equatable {
    var taskId: String
    var taskDescription: String
    var taskTitle: String
    var isCompleted: Bool
}
