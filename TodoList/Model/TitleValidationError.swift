//
//  TitleValidationError.swift
//  TodoList
//
//  Created by Yuriy Burkut on 22.01.2021.
//

import Foundation
import Validator

struct TitleValidationError : ValidationError {
    
    let message: String
    
    public init(_ message: String) {
        self.message = message
    }
}
