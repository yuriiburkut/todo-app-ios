//
//  TaskCoreDataEntity+CoreDataProperties.swift
//  TodoList
//
//  Created by Yuriy Burkut on 28.01.2021.
//
//

import Foundation
import CoreData


extension TaskCoreDataEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TaskCoreDataEntity> {
        return NSFetchRequest<TaskCoreDataEntity>(entityName: "TaskCoreDataEntity")
    }

    @NSManaged public var isCompleted: Bool
    @NSManaged public var taskDescription: String?
    @NSManaged public var taskId: String?
    @NSManaged public var taskTitle: String?
    
    func toTask() -> Task {
        return Task(taskId: taskId ?? UUID.init().uuidString,
                    taskDescription: taskDescription ?? "",
                    taskTitle: taskTitle ?? "",
                    isCompleted: isCompleted)
    }
    
    func updateData(task: Task) {
        if taskId != task.taskId { return }
        
        isCompleted = task.isCompleted
        taskDescription = task.taskDescription
        taskTitle = task.taskTitle
    }

}
