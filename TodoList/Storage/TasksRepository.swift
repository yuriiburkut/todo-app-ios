//
//  TasksRepository.swift
//  TodoList
//
//  Created by Yuriy Burkut on 27.01.2021.
//

import Foundation
import CoreData

let TASK_ENTITY_NAME = "TaskCoreDataEntity"

class TasksRepository {

    let container: NSPersistentContainer
    let backgroundContext: NSManagedObjectContext
    
    init(container: NSPersistentContainer) {
        self.container = container
        self.backgroundContext = container.newBackgroundContext()
    }
    
    func fetchTasks(onResult: @escaping ([Task]) -> Void) {
        let request = NSFetchRequest<TaskCoreDataEntity>(entityName: TASK_ENTITY_NAME)
        let asyncRequest = NSAsynchronousFetchRequest(fetchRequest: request) { asuncResult in
            guard let results = asuncResult.finalResult else {return}
            var tasks = [Task]()
            for result in results {
                tasks.append(result.toTask())
            }
            DispatchQueue.main.async {
                onResult(tasks)
            }
        }
        
        do {
            try backgroundContext.execute(asyncRequest)
        } catch let error {
            print("NSAsynchronousFetchRequest error: \(error)")
        }
    }
    
    
    func saveTask(task: Task) {
        container.performBackgroundTask { context in
            if let newTask: TaskCoreDataEntity =
                NSEntityDescription.insertNewObject(forEntityName: TASK_ENTITY_NAME, into: context) as? TaskCoreDataEntity
            {
                newTask.taskId = task.taskId
                newTask.updateData(task: task)
                self.commitChanges(context: context)
            }
        }
    }
    
    func updateTask(task: Task) {
        guard let entity = getEntityByTaskId(taskId: task.taskId) else {return}
        guard let editedTask = backgroundContext.object(with: entity.objectID) as? TaskCoreDataEntity else {return}
        editedTask.updateData(task: task)
        commitChanges(context: backgroundContext)
    }
    
    func removeTask(task: Task) {
        if let entity = getEntityByTaskId(taskId: task.taskId) {
            backgroundContext.delete(entity)
            commitChanges(context: backgroundContext)
        }
    }
    
    func commitChanges(context: NSManagedObjectContext) {
        if context.hasChanges {
            do {
                try context.save()
                
            } catch {
            fatalError("Failure to save context: \(error)")
            }
        }
    }
    
    private func getEntityByTaskId(taskId: String) -> TaskCoreDataEntity? {
        let request = NSFetchRequest<TaskCoreDataEntity>(entityName: TASK_ENTITY_NAME)
        request.predicate = NSPredicate(format: "taskId MATCHES %@", taskId)
        
        do {
            return try backgroundContext.fetch(request).first
        } catch {
            return nil
        }
    }
}
